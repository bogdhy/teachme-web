import {Component} from '@angular/core';

@Component({
  selector: 'tcm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'teach-me-web';
}
